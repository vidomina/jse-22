package com.ushakova.tm.exception.empty;

import com.ushakova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("An error has occurred: password is empty.");
    }

}
