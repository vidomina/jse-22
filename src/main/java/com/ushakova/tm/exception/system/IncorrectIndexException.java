package com.ushakova.tm.exception.system;

import com.ushakova.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(final String value) {
        super("An error has occurred:" + value + " is not a number.");
    }

    public IncorrectIndexException() {
        super("An error has occurred: index is incorrect.");
    }

}
