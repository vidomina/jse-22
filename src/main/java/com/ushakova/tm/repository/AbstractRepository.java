package com.ushakova.tm.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E findById(final String id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

}
