package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> list = new ArrayList<>();

    @Override
    public void add(AbstractCommand command) {
        list.add(command);
    }

    @Override
    public Collection<String> getArguments() {
        final List<String> args = new ArrayList<>();
        for (final AbstractCommand command : list) {
            if (command.arg() != null)
                args.add(command.arg());
        }
        return args;
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return list.stream()
                .filter(c -> c.arg().equals(arg))
                .findFirst().orElse(null);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return list.stream()
                .filter(c -> c.name().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> names = new ArrayList<>();
        for (final AbstractCommand command : list) {
            names.add(command.name());
        }
        return names;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return list;
    }

}