package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IBusinessRepository;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public E add(final E entity, final String userId) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        entities.removeAll(entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList()));
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(Comparator<E> comparator, final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String id, final String userId) {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    public E findByIndex(final Integer index, final String userId) {
        final E entity = entities.get(index);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    public E findByName(final String name, final String userId) {
        return findAll(userId).stream()
                .filter(e -> e.getName().equals(name))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void remove(final E entity, final String userId) {
        if (userId.equals(entity.getUserId())) {
            entities.remove(entity);
        }
    }

    @Override
    public E removeById(final String id, final String userId) {
        final E entity = findById(userId, id);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index, final String userId) {
        final E entity = findByIndex(index, userId);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

    @Override
    public E removeByName(final String name, final String userId) {
        final E entity = findByName(name, userId);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

}
