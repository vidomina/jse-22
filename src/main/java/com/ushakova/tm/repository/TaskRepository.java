package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId, final String userId) {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

}
