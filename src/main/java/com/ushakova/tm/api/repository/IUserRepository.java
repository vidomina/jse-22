package com.ushakova.tm.api.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    List<User> findAll();

    User findByEmail(final String email);

    User findByLogin(final String login);

    User removeByLogin(final String login);

    User removeUser(final User user);

}
