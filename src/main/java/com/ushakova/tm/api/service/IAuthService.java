package com.ushakova.tm.api.service;

import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.User;

public interface IAuthService {

    void checkRoles(Role... roles);

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(final String login, final String password);

    void logout();

    void registry(final String login, final String password, final String email);

}
