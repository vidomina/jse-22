package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectFindByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by index.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Show Project***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findByIndex(index, userId);
        if (project == null) throw new ProjectNotFoundException();
        showProjectInfo(project);
    }

    @Override
    public String name() {
        return "project-view-by-index";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
