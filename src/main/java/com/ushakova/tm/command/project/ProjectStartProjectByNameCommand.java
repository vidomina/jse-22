package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectStartProjectByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set \"In Progress\" status to project by name.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Set Status \"In Progress\" to Project***\nEnter Project Name:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startByName(name, userId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "start-project-by-name";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
