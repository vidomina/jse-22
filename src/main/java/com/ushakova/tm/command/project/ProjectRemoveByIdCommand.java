package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Remove Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
