package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("***Unlock User***");
        System.out.println("***Enter Login:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("***Ok**");
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
