package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("***Remove User***");
        System.out.println("Enter User Login:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("***Ok***");
    }

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}

