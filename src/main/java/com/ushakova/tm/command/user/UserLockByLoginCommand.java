package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("***Lock User***");
        System.out.println("***Enter Login:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("***Ok**");
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
