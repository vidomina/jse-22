package com.ushakova.tm.command;

import com.ushakova.tm.api.service.IServiceLocator;
import com.ushakova.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public abstract String name();

    public Role[] roles() {
        return null;
    }

    public void setIServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
