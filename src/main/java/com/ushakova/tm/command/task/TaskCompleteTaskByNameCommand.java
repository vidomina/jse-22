package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;

public class TaskCompleteTaskByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set \"Complete\" status to task by name.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().completeByName(name, userId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "complete-task-by-name";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
