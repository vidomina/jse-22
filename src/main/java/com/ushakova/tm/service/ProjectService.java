package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.service.IProjectService;
import com.ushakova.tm.exception.empty.EmptyDescriptionException;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.model.Project;

public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String name, final String description, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
        return project;
    }

}
