package com.ushakova.tm.model;

import com.ushakova.tm.api.entity.IWBS;

public class Project extends AbstractBusinessEntity implements IWBS {

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() + "\nTitle: " + name
                + "\nDescription: " + description
                + "\nStatus: " + status.getDisplayName()
                + "\nStart Date:" + dateStart
                + "\nCreated: " + dateCreate;
    }

}