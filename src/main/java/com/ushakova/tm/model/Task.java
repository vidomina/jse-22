package com.ushakova.tm.model;

import com.ushakova.tm.api.entity.IWBS;

public class Task extends AbstractBusinessEntity implements IWBS {

    private String projectId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectID) {
        this.projectId = projectID;
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() + "\nTitle: " + name
                + "\nDescription: " + description
                + "\nStatus: " + status.getDisplayName()
                + "\nStart Date:" + dateStart
                + "\nCreated: " + dateCreate;
    }

}
